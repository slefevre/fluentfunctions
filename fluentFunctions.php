<?php

namespace fluentFunctions;

class fluentFunctions {

    private $value;

    public function __construct($value) {
        $this->value = $value;
    }

    public function __call($function, $args) {
        array_unshift($args, $this->value);

        // some functions have the argument in the wrong order
        // for our needs. This checks for a replacement.
        if ( function_exists('\fluentFunctions\_' . $function) ) {
            $function = '\fluentFunctions\_' . $function;
        }
        $this->value = call_user_func_array($function,$args);
        return $this;
    }

    public function __get($function) {
        return $this->__call($function, array());
    }

    public function get($value) {
        return new fluentFunctions($value);
    }

    public function __toString() {
        return $this->value;
    }

    public function value() {
        return is_array($this->value) ? serialize($this->value) : $this->value;
    }
}

function _str_replace($subject, $search, $replace) {
    return str_replace($search, $replace, $subject);
}
